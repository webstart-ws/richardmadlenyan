ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [40.191580, 44.510323],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        }),
        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Richard Madlenyan',
        }, {
            // iconLayout: 'default#image',
            // iconImageHref: '',
            // iconImageSize: [100, 100],
            // iconImageOffset: [+30, -20]
        });
    myMap.geoObjects
        .add(myPlacemark)
});